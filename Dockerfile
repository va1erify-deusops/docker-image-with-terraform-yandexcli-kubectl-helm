FROM ubuntu:20.04

#Configure tz-data
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ

RUN apt update
RUN apt install curl unzip -y
RUN rm -rf /var/lib/apt/lists/*

# Install Terraform
RUN curl -L -o terraform.zip https://hashicorp-releases.yandexcloud.net/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip && \
    unzip terraform.zip  && \
    mv terraform /usr/bin/ && \
    rm terraform.zip

# Install Yandex CLI
RUN curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash -s -- -a

# Install kubectl
RUN curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

# Ingtall Helm
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh

# Add custom paths to PATH
ENV PATH="/usr/bin:/usr/local/bin:/root/yandex-cloud/bin:${PATH}"

CMD ["/bin/bash"]